/*
Write a method to build a tree from the right parenthetic string representation (return the root
node of the tree) and a method to construct the left parenthetic string representation of a tree
represented by the root node this. Tree is a pointer structure with two pointers to link nodes of
type  Node - pointer to the first child and pointer to the next sibling. Build test trees and print
 the results in main-method, do not forget to test a tree that consists of one node only. Node name
  must be non-empty and must not contain round brackets, commas and whitespace symbols. In case of
  an invalid input string the  parsePostfix method must throw a  RuntimeException with meaningful error message.

public static Node parsePostfix (String s)
public String leftParentheticRepresentation()

Koostage meetodid puu parempoolse suluesituse (String) järgi puu moodustamiseks (parsePostfix, tulemuseks puu
juurtipp) ning etteantud puu vasakpoolse suluesituse leidmiseks stringina (puu juureks on tipp  this, meetod
leftParentheticRepresentation tulemust ei trüki, ainult tagastab String-tüüpi väärtuse). Testige muuhulgas ka
ühetipuline puu. Testpuude moodustamine ja tulemuse väljatrükk olgu peameetodis. Puu kujutamisviisina kasutage
viidastruktuuri (viidad "esimene alluv"  firstChild ja "parem naaber"  nextSibling, tipu tüüp on  Node). Tipu nimi
ei tohi olla tühi ega sisaldada ümarsulge, komasid ega tühikuid. Kui meetodile  parsePostfix etteantav sõne on vigane,
siis tuleb tekitada asjakohase veateatega  RuntimeException.
 */


import java.util.*;

/*
TODO:
 *Teha kindlaks, et kasutaja sisend string on õige(kas teha seda esialgu eraldi meetodis, et hoida põhi parsePostfix
 algoritmi puhtana?):
      *parenthesis counteriga nt loeme kokku, et oleks võrdne arv avamis ja lõpetamis sulgusid.
      *Teeme kindlaks, et poleks kõrvuti kaks koma
      *kontrollime, et empty subtree viskaks veateate - "()A" ehk siis, et sulgude sees oleks midagi
      *Tipu nimi ei tohi olla tühi ega sisaldada ümarsulge, komasid ega tühikuid
 *Võtta kasutaja sisend postfix (parempoolne suluestilus) kujult ning konstrueerida sellest vastav puu
 *Teha konstrueeritud puust prefix kujul olev string ja tagastada see
 *Laisalt tehes saaks RPN meetodit ära kasutada, ainult numbrid ja märgid alles jätta...

 /

 */

public class Node {

   private String name;
   private Node firstChild;
   private Node nextSibling;

   Node (String n, Node d, Node r) {
      // TODO!!! Your constructor here
      // kas konstruktoris olev kood on õige? Esimese raksuga tundub olevat kuna
      // klassi Node konstruktori päises olevad sisendargumendid on õppejõu poolt pandud
      // ning need peavad viitama millegile või olema väärtustatud.
      this.name = n;
      this.firstChild = d;
      this.nextSibling = r;
   }
   // See on pmst nagu kt5-algorithms2019 buildFromRPN aga postfix kujul, ehk siis RPN - reverse polish notation
   // kujul sulgudega
   public static Node parsePostfix (String s) {


      return null;  // TODO!!! return the root
   }

   public String leftParentheticRepresentation() {
      // Sisendiks ei pea midagi võtma, kasutame this keywordi, et ligi pääseda olemasolevale
      // puu tipule - this.
      return "";
      // TODO!!! return the string without spaces
   }

   public static void main (String[] param) {
      String s = "(B1,C)A";
      Node t = Node.parsePostfix (s);
      String v = t.leftParentheticRepresentation();
      System.out.println (s + " ==> " + v); // (B1,C)A ==> A(B1,C)
   }

//Muuta see ümber, et too tuvastaks ka tähed ära!
   public static boolean isNumeric (String str) {
      try{
         double d = Double.parseDouble(str);
      } catch (NumberFormatException | NullPointerException nfe ){
         return false;
      }
      return true;
   }
   public static boolean isAcceptedLetters (String str) {
      return true;
   }

   public static boolean isOperator(String str){
      if (str.equals("+") || str.equals("-") || str.equals("*") || str.equals("/") || str.equals("SWAP") || str.equals("ROT")) {
         return true;
      } else return false;
   }

   public static String[] isValid (String pol){
      if (pol == null || pol.isEmpty()) throw new RuntimeException("The input field is empty");

      //We clean out the input first we use the string tokenizer method
      //TODO: fix maybe needed - right now we split the string into an array using StringTokenizer
      // which splits our input into an array after every space. We now actually need to split on
      // commas and parenthesis I think
      StringTokenizer st = new StringTokenizer(pol, " ");
      String[] stringArr = new String[st.countTokens()];
      int count = 0;

      if(!st.hasMoreElements()) throw new RuntimeException("The input field contained only spaces");


      while (st.hasMoreElements()){
         String potentialAllowedValue = st.nextToken();

         if(isNumeric(potentialAllowedValue) || isOperator(potentialAllowedValue)){
            stringArr[count] = potentialAllowedValue;
            count++;
         } else {
            throw new RuntimeException("Incorrect arguments given by user.\n User input: " + pol +
                    " Detected wrong argument: " + potentialAllowedValue );
         }
      }
      //check for too few or too many numbers
      howManyNumbers(stringArr, pol);
      //TODO: check for too many commas, parenthesis - should
      // we make a method for these to abstract away code from here
      return stringArr;
   }

   public static void howManyNumbers (String[] polArr, String pol){
      int numCounter = 0;
      int operatorCounter = 0;

      for(int i = 0; i < polArr.length; i++){
         if(isNumeric(polArr[i])){
            numCounter++;
         }else if(isOperator(polArr[i])){
            if(polArr[i].equals("SWAP") || polArr[i].equals("ROT")){
            } else{
               operatorCounter++;
            }
         }
      }
      if (!isNumeric(polArr[0])){
         throw new RuntimeException("First element in the user input cannot be an operand. User input: " + pol);
      }

      if (polArr.length > 2 && isNumeric(polArr[polArr.length - 1])){
         throw new RuntimeException("Last element in the user input cannot be a number. User input: " + pol);
      }

      if (numCounter <= operatorCounter){
         throw new RuntimeException("Too few numbers, user input error. User input: " + pol);
      }
      if(numCounter - operatorCounter > 1){
         throw new RuntimeException("Too many numbers, user input error. User input: " + pol);
      }
   }

}

